<!DOCTYPE html>
    <html>
        <head>
            <title>Calculation Result</title>
            <style type="text/css">
                body{
                    width: 760px;
                    background-color: teal;
                    margin: 0 auto;
                    padding: 0;
                    font:12px/16px Verdana, sans-serif;
                    text-align: center;
                }
                div#main{
                    background-color: #FFF;
                    margin: 0;
                    padding: 10px;
                }
    </style>
        </head>
        <body>
            <h1><br><br><br>
            <?php
            $num1 = $_GET['var1'];
            $num2 = $_GET['var2'];
            $operation = $_GET['Operation'];
            if($operation == "Add") {
                echo "Answer = ".htmlentities($num1+$num2);
            }
            else if ($operation == "Sub") {
                echo "Answer = ".htmlentities($num1-$num2);
            }
            else if ($operation == "Mul") {
                echo "Answer = ".htmlentities($num1*$num2);
            }
            else if ($operation == "Div") {
                if ($num2 == 0){
                    echo "Divide by 0 error";
                }
                else {
                    echo "Answer = ".htmlentities($num1/$num2);
                }
            }
            else {
                echo "No valid operation selected";
            }
            ?>
            </h1>
        </body>
    </html>